<?php 
class indexController
{
    private $Registrossigma;
    public $id_registro;
    public function __construct(){

		try {
			$this->Registrossigma= new Registrossigma();

		} catch (Exception $e) {
			die($e->getMessage());
		}
    }
    public function index(){
        require_once("view/all/header.php");
        require_once("view/index.php");
        require_once("view/all/footer.php");
    }
    public function guardarData(){
    $this->Registrossigma=new Registrossigma();
		$this->Registrossigma->saveRegistro($_REQUEST['nombre'],$_REQUEST['email'],$_REQUEST['departamento'],$_REQUEST['cuidad']);
		 header('location:?c=Index&m=adminView');

    }
    public function editarData(){
      $this->Registrossigma=new Registrossigma();
      $this->Registrossigma->editarRegistro($_REQUEST['id_registro'],$_REQUEST['nombre'],$_REQUEST['email'],$_REQUEST['departamento'],$_REQUEST['cuidad']);
        	header('Location: ?c=Index&m=adminView');
    }
    public function eliminarData(){
      $this->Registrossigma->deleteRegistro($_REQUEST['id_registro']);
		header('location:?c=Index&m=adminView');
      
    }
    public function adminView(){
      require_once("view/all/header.php");
      require_once("view/admin.php");
      require_once("view/all/footer.php");
    }
    public function viewEdit(){
   
      if (isset($_REQUEST['id_registro'])) {
         $this->id_registro=$_REQUEST['id_registro'];
   
       
			}
      require_once("view/all/header.php");
      require_once("view/adminedit.php");
      require_once("view/all/footer.php");
      
    }
    }



?>

