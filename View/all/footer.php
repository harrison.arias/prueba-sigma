<script src="<?php echo url_assets?>js/validacionf.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script> 
$('.selectdepartamento').on('change', function() {
  
  var datos = <?php echo json_encode($items, TRUE);?>;
  var department = $(this).val();
  var cities;

  for (const key in datos) {
      if (datos.hasOwnProperty(key)) {
          if(key == department) {
            cities = datos[key];
            break;
          }
      }
  }
  
  $("#Coloquesucuidad").empty();

  for (let index = 0; index < cities.length; index++) {
      const element = cities[index];
      document.getElementById("Coloquesucuidad").innerHTML += "<option value='"+element+"'>"+element+"</option>"; 
  }

  
});
</script>
</body>
</html>