<div class="container">
<div class="alert alert-primary" role="alert">
  <a class="btn btn-info" href="?c=Index&m=index">Agregar Usuario nuevo</a>
</div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      
      <th scope="col">nombre</th>
      <th scope="col">correo</th>
      <th scope="col">departamento</th>
      <th scope="col">cuidad</th>
      <th scope="col">Editar</th>
      <th scope="col">Eliminar</th>
    </tr>
  </thead>
  <tbody>
  <?php 
	       	foreach ($this->Registrossigma->ObtenerRegistros() as $result ) {
               ?>
    <tr>
      <td><?php echo $result->nombre; ?></td>
      <td><?php echo $result->correo; ?></td>
      <td><?php echo $result->departamento; ?></td>
      <td><?php echo $result->cuidad; ?></td>
      <td><a href="?c=Index&m=viewEdit&id_registro=<?php echo $result->id_registro; ?>"  class="btn btn-success" ><span class="fas fa-pencil"></span></a></td>
      <td><a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" class="btn btn-warning" href="?c=Index&m=eliminarData&id_registro=<?php echo $result->id_registro; ?>"><span class="fas fa-trash-alt" aria-hidden="true"> </span></a></td>
    </tr>
    
    <?php }?>
  </tbody>
</table>
</div>